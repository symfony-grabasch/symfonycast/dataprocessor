# Data Processor with API-Platform
This repository shows an example how to use plain password converting to hashed password via Processor Interface from API-Platform.
## Used Version
- Symfony 6.1
- API-Platform 3.0

## How to install
`git clone https://gitlab.com/symfony-grabasch/symfonycast/dataprocessor.git`

`cd dataprocessor`

`composer install`

`sudo nano .env.local`

Insert line `DATABASE_URL="mysql://USERNAME:PASSWORD@127.0.0.1:3306/DBNAME?serverVersion=8&charset=utf8mb4"` Maybe you have a different Database setup. Please customize what you need. And Save the file.

If you don't have created the DB before please run `php bin/console doctrine:database:create`

`php bin/console doctrine:migrations:migrate`

`symfony serve`

Navgiate to https://127.0.0.1:8000/api (Maybe a different port) and test some request e.g. 


curl -X 'POST' \
  'https://localhost:8000/api/users' \
  -H 'accept: application/ld+json' \
  -H 'Content-Type: application/ld+json' \
  -d '{
  "email": "daniel@example.com",
  "password": "123456"
}'


## Symfony Cast
You will find the video of this lexion here:
https://symfonycasts.com/screencast/api-platform-security/encode-user-password

## License

### [MIT](https://opensource.org/licenses/MIT)

Created for SymfonyCast members who love symfony like me ♥

STAND WITH</span> <span style="color:blue">UKR</span><span style="color:yellow">AINE</span>